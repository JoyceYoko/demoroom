package app.rayell.com.demoroom.extensions

import android.app.Activity
import android.app.Dialog
import android.support.annotation.LayoutRes
import android.view.Window


fun Activity.showDialog(@LayoutRes layout: Int, actionWhenDialogDismiss: (() -> Unit?)? = null) {
    val dialog = Dialog(this)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.setContentView(layout)
    dialog.setCanceledOnTouchOutside(true)

    actionWhenDialogDismiss?.let { dismissAction ->
        dialog.setOnCancelListener {
            dismissAction()
        }
    }


    dialog.show()
}