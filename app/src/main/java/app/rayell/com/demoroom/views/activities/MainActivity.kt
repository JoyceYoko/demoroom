package app.rayell.com.demoroom.views.activities

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Toast
import app.rayell.com.demoroom.R
import app.rayell.com.demoroom.adapters.MovieAdapter
import app.rayell.com.demoroom.database.model.MovieDb
import app.rayell.com.demoroom.extensions.RecyclerItemClickListener
import app.rayell.com.demoroom.extensions.onItemClick
import app.rayell.com.demoroom.viewmodels.MovieViewModel
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.yesButton
import org.koin.android.viewmodel.ext.android.viewModel

const val NEW_MOVIE_ACTIVITY_REQUEST_CODE = 1
const val LIBELLE_KEY_EXTRA = "libelle_movie"

class MainActivity : AppCompatActivity() {

    private val mMovieViewModel: MovieViewModel by viewModel()
    private lateinit var recyclerView: RecyclerView
    private lateinit var floatButtonAdd: FloatingActionButton
    private lateinit var listLytEmpty: ConstraintLayout

    private lateinit var adapter: MovieAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar: android.support.v7.widget.Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        initComponents()
        adapter = MovieAdapter(this)

        initRecyclerView()

        initObserver()

        initListeners()
    }

    private fun initObserver() {
        mMovieViewModel.getAllMovies().observe(this,
            Observer<List<MovieDb>> { listMovie ->
                listMovie?.let {
                    if (listMovie.isNotEmpty()) {
                        listLytEmpty.visibility = View.GONE
                        recyclerView.visibility = View.VISIBLE
                        adapter.setDataListMovies(it)
                    } else {
                        listLytEmpty.visibility = View.VISIBLE
                        recyclerView.visibility = View.GONE
                    }
                }
            })
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == NEW_MOVIE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            val movie = MovieDb(title = data.getStringExtra(LIBELLE_KEY_EXTRA))
            mMovieViewModel.insert(movie)
        } else if (requestCode == NEW_MOVIE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(applicationContext, "Une erreur est survenue", Toast.LENGTH_LONG).show()
        }
    }

    private fun initRecyclerView() {
        recyclerView.adapter = adapter
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
    }

    private fun initComponents() {
        recyclerView = findViewById(R.id.myListMovies)
        floatButtonAdd = findViewById(R.id.floatButtonAdd)
        listLytEmpty = findViewById(R.id.list_lyt_empty)
    }

    private fun initListeners() {
        floatButtonAdd.setOnClickListener {
            val intent = Intent(this@MainActivity, NewMovieActivity::class.java)
            startActivityForResult(intent, NEW_MOVIE_ACTIVITY_REQUEST_CODE)
        }

        recyclerView.onItemClick(object : RecyclerItemClickListener.OnClickListener {
            override fun onItemClick(position: Int, view: View) {
                adapter.getItemAtPosition(position)?.let { item ->
                    alert(title = "Information", message = "Etes-vous sur de vouloir supprimer ce film?") {
                        yesButton {
                            mMovieViewModel.removeItem(item)
                        }
                        noButton {}
                    }.show()

                }
            }
        })
    }
}
