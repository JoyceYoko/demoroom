package app.rayell.com.demoroom.database.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "table_movie")
data class MovieDb(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val title : String)