package app.rayell.com.demoroom.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import app.rayell.com.demoroom.database.model.MovieDb

@Database(entities = [MovieDb::class], version = 1, exportSchema = false)
abstract class DatabaseMovie : RoomDatabase() {
    abstract fun movieDao(): Dao

    companion object {
        @Volatile
        private var INSTANCE: DatabaseMovie? = null

        fun getDatabase(context: Context): DatabaseMovie {
            if (INSTANCE == null) {
                synchronized(this) {
                    val instance = Room.databaseBuilder(
                        context.applicationContext,
                        DatabaseMovie::class.java,
                        "movie_database.db"
                    ).build()
                    INSTANCE = instance

                }

            }
            return INSTANCE!!
        }
    }

}