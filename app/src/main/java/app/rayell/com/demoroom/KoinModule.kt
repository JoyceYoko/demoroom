package app.rayell.com.demoroom

import android.arch.persistence.room.Room
import app.rayell.com.demoroom.database.DatabaseMovie
import app.rayell.com.demoroom.database.MovieRepository
import app.rayell.com.demoroom.viewmodels.MovieViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.module


val daoModule: Module = module {
    single {
        Room.databaseBuilder(androidApplication(), DatabaseMovie::class.java, "movie_database.dbb").build()
    }
    single { get<DatabaseMovie>().movieDao() }
    single { MovieRepository(get()) }
}

val viewModelModule: Module = module {
    viewModel { MovieViewModel(androidApplication(), get()) }
}
