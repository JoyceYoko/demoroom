package app.rayell.com.demoroom

import android.app.Application
import org.koin.android.ext.android.startKoin


class DemoRoomApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(viewModelModule, daoModule))
    }
}