package app.rayell.com.demoroom.views.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import app.rayell.com.demoroom.R

class NewMovieActivity : AppCompatActivity() {

    private lateinit var mEditWordView: EditText
    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.new_movie_activity)

        initComponents()
        initListeners()
    }

    private fun initListeners() {
        button.setOnClickListener {
            val replyIntent = Intent()
            if (TextUtils.isEmpty(mEditWordView.getText())) {
                setResult(Activity.RESULT_CANCELED, replyIntent)
            } else {
                val movie = mEditWordView.getText().toString()
                replyIntent.putExtra(LIBELLE_KEY_EXTRA, movie)
                setResult(Activity.RESULT_OK, replyIntent)
            }
            finish()
        }
    }

    private fun initComponents() {
        mEditWordView = findViewById(R.id.edit_word)
        button = findViewById(R.id.button_save)
    }
}