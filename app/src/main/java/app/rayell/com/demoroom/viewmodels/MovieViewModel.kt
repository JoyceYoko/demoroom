package app.rayell.com.demoroom.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import app.rayell.com.demoroom.database.MovieRepository
import app.rayell.com.demoroom.database.model.MovieDb
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext


/**
 * Fournit les données à l'interface utilisateur et effectue les traitment necessaire sur les données
 */
class MovieViewModel(application: Application, private val repository: MovieRepository) :
    AndroidViewModel(application) {

    private var parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    fun insert(movie: MovieDb) = scope.launch(Dispatchers.IO) {
        repository.insert(movie)
    }

    fun removeItem(movie: MovieDb) = scope.launch(Dispatchers.IO) {
        repository.delete(movie)
    }

    fun getAllMovies() = repository.allMovies

    override fun onCleared() {
        super.onCleared()
        parentJob.cancel()
    }
}