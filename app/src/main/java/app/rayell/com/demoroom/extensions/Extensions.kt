package app.rayell.com.demoroom.extensions

import android.support.v7.widget.RecyclerView
import android.view.View


fun RecyclerView.onItemClick(listener: RecyclerItemClickListener.OnClickListener) {
    this.addOnChildAttachStateChangeListener(
        RecyclerItemClickListener(
            this,
            listener
        )
    )
}

fun RecyclerView.onLongItemClick(listener: RecyclerItemClickListener.OnLongClickListener) {
    this.addOnChildAttachStateChangeListener(
        RecyclerItemClickListener(
            this,
            longClickListener = listener
        )
    )
}


class RecyclerItemClickListener(private val mRecycler: RecyclerView, private val clickListener: OnClickListener? = null, private val longClickListener: OnLongClickListener? = null) :
    RecyclerView.OnChildAttachStateChangeListener {

    override fun onChildViewDetachedFromWindow(view: View?) {
        view?.setOnClickListener(null)
        view?.setOnLongClickListener(null)
    }

    override fun onChildViewAttachedToWindow(view: View?) {
        view?.setOnClickListener { v ->
            setOnChildAttachedToWindow(v)
        }
    }

    private fun setOnChildAttachedToWindow(view: View?) {
        view?.let {
            val position = mRecycler.getChildLayoutPosition(it)
            if (position >= 0) {
                clickListener?.let { listener ->
                    listener.onItemClick(position, it)
                }
                longClickListener?.onLongItemClick(position, it)
            }
        }
    }

    interface OnClickListener {
        fun onItemClick(position: Int, view: View)
    }

    interface OnLongClickListener {
        fun onLongItemClick(position: Int, view: View)
    }
}