package app.rayell.com.demoroom.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import android.arch.persistence.room.Dao
import app.rayell.com.demoroom.database.model.MovieDb

@Dao
interface Dao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movie: MovieDb)

    @Update
    fun update(movie: MovieDb)

    @Delete
    fun delete(movie: MovieDb)

    @Query("SELECT * FROM table_movie WHERE title == :title")
    fun getMovieByTitle(title : String): LiveData<List<MovieDb>>

    @Query("SELECT * FROM table_movie")
    fun getMovies(): LiveData<List<MovieDb>>
}