package app.rayell.com.demoroom.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import app.rayell.com.demoroom.R
import app.rayell.com.demoroom.database.model.MovieDb


class MovieAdapter(context: Context) : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    private var mInflater: LayoutInflater
    private var mListMovies: List<MovieDb>? = null

    init {
        mInflater = LayoutInflater.from(context)
    }

    inner class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val movieItemView: TextView = itemView.findViewById(R.id.txtTitleMovie)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MovieViewHolder {
        val itemView = mInflater.inflate(R.layout.cell_movie, parent, false)
        return MovieViewHolder(itemView)
    }

    fun getItemAtPosition(pos: Int): MovieDb? {
        return mListMovies?.get(pos)
    }

    override fun getItemCount(): Int {
        mListMovies?.let {
            return it.size
        } ?: kotlin.run { return 0 }
    }

    fun setDataListMovies(movies: List<MovieDb>) {
        mListMovies = movies
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        mListMovies?.let {
            val current = it[position]
            holder.movieItemView.text = current.title
        }
    }

}