package app.rayell.com.demoroom.extensions

import android.app.Fragment


fun Fragment.showCoachmarkDialog(layout: Int, actionWhenDialogDismiss: (() -> Unit?)? = null) {
    activity?.showDialog(layout, actionWhenDialogDismiss)
}