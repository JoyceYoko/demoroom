package app.rayell.com.demoroom.database

import android.arch.lifecycle.LiveData
import android.support.annotation.WorkerThread
import app.rayell.com.demoroom.database.model.MovieDb

/**
 *  Equivalent du manager
 *  Un référentiel gère les requêtes et vous permet d'utiliser plusieurs moteurs. Dans l'exemple le plus courant, le référentiel implémente
 *  la logique permettant de choisir de récupérer les données d'un réseau ou d'utiliser les résultats mis en cache dans une base de données locale.
 */
class MovieRepository(private val movieDao: Dao){

    val allMovies: LiveData<List<MovieDb>> = movieDao.getMovies()

    @WorkerThread  //appelée à partir d'un thread non-UI
    fun insert(movie: MovieDb) {
        movieDao.insert(movie)
    }

    @WorkerThread
    fun update(movie: MovieDb) {
        movieDao.update(movie)
    }

    @WorkerThread
    fun delete(movie: MovieDb) {
        movieDao.delete(movie)
    }

    fun getMovieByTitle(title: String) {
        movieDao.getMovieByTitle(title)
    }
}